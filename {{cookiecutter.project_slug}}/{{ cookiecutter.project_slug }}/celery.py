import os
import sys
from celery import Celery
from celery.schedules import crontab

# На винде не работает мультипроцессинг
if sys.platform == 'win32':
    os.environ.setdefault('FORKED_BY_MULTIPROCESSING', '1')

os.environ.setdefault('DJANGO_SETTINGS_MODULE', '{{ cookiecutter.project_slug }}.settings')

celery_app = Celery('{{ cookiecutter.project_slug }}')
celery_app.config_from_object('django.conf:settings', namespace='CELERY')
celery_app.autodiscover_tasks()

celery_app.conf.update(
    result_expires=3600,
    enable_utc=False,
    timezone='{{ cookiecutter.timezone }}'
)

celery_app.conf.beat_schedule = {

}
