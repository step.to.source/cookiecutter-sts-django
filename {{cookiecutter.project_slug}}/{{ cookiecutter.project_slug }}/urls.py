from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from .settings.packages.local import drf_yasg
from .settings.packages.base import djoser_rest_jwt, restframework

admin.sites.AdminSite.site_header = 'Панель администратора {{ cookiecutter.project_slug }}'
admin.sites.AdminSite.site_title = 'Панель администратора {{ cookiecutter.project_slug }}'

urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += restframework.get_url_patterns()
urlpatterns += djoser_rest_jwt.get_url_patterns()

admin.autodiscover()

if settings.DEBUG:
    urlpatterns += drf_yasg.get_url_patterns()
