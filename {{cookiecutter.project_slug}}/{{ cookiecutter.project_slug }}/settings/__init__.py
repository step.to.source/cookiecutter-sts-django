""" Организация настроек слейдует концепции - установил модуль скопируй файл
настройки и определи его в __init__.py.
Особенности:
    Порядок определения модулей в большинстве случев не имеет значения, но могут
    встречаться случаи, когда он должен учитываться.

    Для уменьшения зависимости с проектом, urlpatterns определяется через
    функцию get_url_patterns и вызывается в проекте. Сделано это для того, чтоб
    иметь возможность импортировать дополнительные модули при импорте. Если
    импорт делать вверху каждоого файла настроек, то импорты могут конфликтовать
"""
import environ

root = environ.Path(__file__) - 3  # get root of the project
env = environ.Env(DEBUG=(bool, True))
DEBUG = env('DEBUG')

print(f'Currently used settings for DEBUG = {DEBUG}')

if DEBUG:
    environ.Env.read_env(root('.env.local'))  # reading .env.local file
    from .local import *
    from .packages.base import *
    from .packages.local import *
else:
    environ.Env.read_env(root('.env.production'))
    from .production import *
    from .packages.base import *
    from .packages.prod import *
