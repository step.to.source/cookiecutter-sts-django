from ... import env

CORS_ORIGIN_WHITELIST = env.list('CORS_ORIGIN_WHITELIST', default='localhost')
