from ...base import (INSTALLED_APPS,
                     MIDDLEWARE)

INSTALLED_APPS.append('corsheaders')
MIDDLEWARE += [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.BrokenLinkEmailsMiddleware',
    'django.middleware.common.CommonMiddleware',
]
