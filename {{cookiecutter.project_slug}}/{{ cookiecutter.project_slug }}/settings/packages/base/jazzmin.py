from ...base import INSTALLED_APPS

pos = INSTALLED_APPS.index('django.contrib.admin')
INSTALLED_APPS.insert(pos, 'jazzmin')

JAZZMIN_SETTINGS = {
    # title of the window (Will default to current_admin_site.site_title if absent or None)
    "site_title": "US Marketplace API",

    # Title on the brand, and login screen (19 chars max) (defaults to current_admin_site.site_header if absent or None)
    "site_header": "US Marketplace API",

    # square logo to use for your site, must be present in static files, used for favicon and brand on top left
    # "site_logo": "",

    # Welcome text on the login screen
    "welcome_sign": "Welcome to the US Marketplace API",

    # Copyright on the footer
    "copyright": "US Marketplace API",

    # Field name on user model that contains avatar image
    # "user_avatar": 'avatar',

    ############
    # Top Menu #
    ############

    # Links to put along the top menu
    "topmenu_links": [

        # Url that gets reversed (Permissions can be added)
        {"name": "Home", "url": "admin:index"},

    ],

    #############
    # CustomUser Menu #
    #############

    # Additional links to include in the user menu on the top right ("app" url type is not allowed)
    "usermenu_links": [
        {"name": "Support", "url": "https://github.com/farridav/django-jazzmin/issues",
         "new_window": True},
        {"model": "apps.users.user"}
    ],

    #############
    # Side Menu #
    #############

    # Whether to display the side menu
    "show_sidebar": True,

    # Whether to aut expand the menu
    "navigation_expanded": False,

    # Hide these apps when generating side menu e.g (auth)
    "hide_apps": ["auth"],

    # Hide these models when generating side menu (e.g auth.user)
    "hide_models": [],

    # List of apps (and/or models) to base side menu ordering off of (does not need to contain all apps/models)
    "order_with_respect_to": ["users"],

    # Custom links to append to app groups, keyed on app name

    # Custom icons for side menu apps/models See https://fontawesome.com/icons?d=gallery&m=free
    # for a list of icon classes
    "icons": {
        "users": "fas fa-users-cog",
        "users.Admin": "fas fa-user",
        "sites": "fas fa-sitemap",
        "authtoken": "fas fa-key",
    },
    # Icons that are used when one is not manually specified
    "default_icon_parents": "fas fa-chevron-circle-right",
    "default_icon_children": "fas fa-circle",

    #################
    # Related Modal #
    #################
    # Use modals instead of popups
    "related_modal_active": False,

    #############
    # UI Tweaks #
    #############
    # Relative paths to custom CSS/JS scripts (must be present in static files)
    # "custom_css": '',
    "custom_js": None,
    # Whether to show the UI customizer on the sidebar
    "show_ui_builder": False,

    ###############
    # Change view #
    ###############
    # Render out the change view as a single form, or in tabs, current options are
    # - single
    # - horizontal_tabs (default)
    # - vertical_tabs
    # - collapsible
    # - carousel
    "changeform_format": "horizontal_tabs",

}

JAZZMIN_UI_TWEAKS = {
    "sidebar_nav_child_indent": True,
}
