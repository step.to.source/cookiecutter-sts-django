from ...base import INSTALLED_APPS

INSTALLED_APPS.append('rest_framework')
REST_FRAMEWORK = {
    "DEFAULT_PERMISSION_CLASSES": [
        'rest_framework.permissions.AllowAny', ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_simplejwt.authentication.JWTAuthentication',
        "rest_framework.authentication.SessionAuthentication",
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',

}


# Do not forget to add this in urls by
# urlpatterns += restframework.get_url_patterns()
def get_url_patterns():
    from django.urls import path, include
    urlpatterns = [
        path('api-auth/', include('rest_framework.urls'))
    ]
    return urlpatterns
