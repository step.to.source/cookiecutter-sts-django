from django.urls import path
from ...base import INSTALLED_APPS

INSTALLED_APPS.append('drf_yasg')

SWAGGER_SETTINGS = {
    'SHOW_REQUEST_HEADERS': True,
    'SECURITY_DEFINITIONS': {
        'Bearer': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization'
        },
    },
    'USE_SESSION_AUTH': True,
    'JSON_EDITOR': True,
    'LOGIN_URL': '/api-auth/login/',
    'LOGOUT_URL': '/api-auth/logout/'

}


# Do not forget to add this in urls by
# urlpatterns += drf_yasg.get_url_patterns()
def get_url_patterns():
    from rest_framework import permissions
    from drf_yasg.views import get_schema_view
    from drf_yasg import openapi

    schema_view = get_schema_view(
        openapi.Info(
            title="Very good API",
            default_version='v1',
            contact=openapi.Contact(email="{{ cookiecutter.email }}"),
            license=openapi.License(name="{{ cookiecutter.open_source_license }}"),
        ),
        public=True,
        permission_classes=(permissions.AllowAny,),
    )
    # Do not forget to add this in urls by urlpatterns += drf_yasg.urlpatterns
    urlpatterns = [
        path('api/v1/swagger/', schema_view.with_ui('swagger', cache_timeout=0),
             name='schema-swagger-ui'),
        path('api/v1/redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    ]
    return urlpatterns
