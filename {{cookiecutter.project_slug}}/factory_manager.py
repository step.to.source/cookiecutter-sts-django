"""
Command line util for creation fixtures in data base
"""
import argparse
import inspect
import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', '{{ cookiecutter.project_slug }}.settings')
django.setup()

description = '''
Util for creation fixtures in data base
'''

args_parser = argparse.ArgumentParser(description=description)
args_parser.add_argument('-a', '--create-all', help='Creating all fixtures',
                         action='store_true', dest='create_all')

def run_all_factories_in_module(module):
    """ Execute all factories in module which names endswith 'Factory' """
    for name, obj in inspect.getmembers(module):
        if inspect.isclass(obj) and name.endswith('Factory'):
            print(f'Running factory: {name}')
            instance = obj()
            print(f'Object ready: {instance}')
            print('-' * 79)


args = args_parser.parse_args()
if args.create_all:
    pass
    # Insert here your modules with fixtures you want to be created
    # Example:
    # from apps.users.tests import factories as users_factories
    # run_all_factories_in_module(users_factories)
