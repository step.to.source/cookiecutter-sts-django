# {{cookiecutter.project_name}}

{{ '=' * cookiecutter.project_name|length }}

**{{cookiecutter.description}}**


## Overview
Project structure minimize headache with environment variables for development 
and for production. Everything working from the box no mater how you start project from docker or locally.

Project uses `pipenv` for convenient way package management

Project uses module conception of settings. The idea is to install and config third party packages in easy way and in 
one place:  

- pipenv install <<package_name>>
- copy and paste settings file from your other project  

No more need to search third party package settings in 1000 lines settings from other project.

Moreover project understands how it starts and use proper settings..

## Basic Commands

### Start project locally for development

Install using `pip`...

    pip install pipenv

Create virtual env

    pipenv shell
    
Install development dependencies

    pipenv install --dev
    
Please do not forget `--dev` flag, because project suggests it started locally for development and require dev packages
    
### Start project in docker for development

On the machine must be installed Docker and Docker compose (refer docs for u'r os)

Then type ...

    docker-compose up -d
    
This will use settings for development with `docker-compose.yml` and `docker-compose.override.yml`

All containers have no open ports except NGINX which act as reverse proxy on port "8000".  
This prevent port conflicts with other projects containers.  
This way is convenient if you use one server for many dev projects. Just change port in `docker-compose.override.yml`

    nginx:
    build:
      context: .
      dockerfile: ./compose/local/nginx/Dockerfile
    ports:
      - 8000:80 # Please use port convenient  for your dev server


### Start project in docker for production

On the machine must be installed Docker and Docker compose (refer docs for u'r os)

Then type ...

     docker-compose.exe -f .\docker-compose.yml -f .\docker-compose.prod.yml up -d
     
This will use settings for production with `docker-compose.yml` and `docker-compose.override.yml`

Prod server initially configured to get SSL certificate.
When you start prod server with NGINX settings to get SSL certificate, just uncomment NGINX settings for
SSL usage and restart containers ...

    docker-compose down
    
    docker-compose.exe -f .\docker-compose.yml -f .\docker-compose.prod.yml up -d


## Licence 

{% if cookiecutter.open_source_license != "Not open source" %}
{{cookiecutter.open_source_license}}
{% endif %}